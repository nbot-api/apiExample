/**
 * 
 */
package com.netease.nbot.apiExample.sign.md5;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netease.nbot.apiExample.consts.Consts;

/**
 * Md5工具类
 * 
 * @date 2016年7月11日
 * @author hzlipengzhen@corp.netease.com
 */
public class MD5Util {
    private static final Logger LOG = LoggerFactory.getLogger(MD5Util.class);
    private static MessageDigest md5;
    static {
        try {
            md5 = MessageDigest.getInstance(Consts.MD5);
        } catch (NoSuchAlgorithmException e) {
            LOG.error("", e);
        }
    }

    /**
     * 计算一个String的md5
     * 
     * @param str
     * @return
     */
    public static String calcMD5(String str) {
        byte[] bytes;
        synchronized (md5) {
            try {
                bytes = md5.digest(str.getBytes(Consts.UTF8));
            } catch (UnsupportedEncodingException e) {
                bytes = md5.digest(str.getBytes());
            }
        }
        StringBuilder ret = new StringBuilder(bytes.length << 1);
        for (int i = 0; i < bytes.length; i++) {
            ret.append(Character.forDigit((bytes[i] >> 4) & 0xf, 16));
            ret.append(Character.forDigit(bytes[i] & 0xf, 16));
        }
        return ret.toString();
    }
}
