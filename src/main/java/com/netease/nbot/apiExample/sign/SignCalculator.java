package com.netease.nbot.apiExample.sign;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netease.nbot.apiExample.consts.Consts;
import com.netease.nbot.apiExample.sign.md5.MD5Util;
import com.netease.nbot.apiExample.util.CollectionUtil;


/**
 * @date 2016年7月11日
 * @author hzlipengzhen@corp.netease.com
 */
public class SignCalculator {
    private static final Logger LOG = LoggerFactory.getLogger(SignCalculator.class);

    /**
     * 计算签名值
     * 
     * @param url 请求的uri
     * @param requestMethod 请求的方法名 method.getName()
     * @param consumerScrete 产品私钥
     * @param params 其他请求参数列表
     * @return
     */
    public static String getSign(String url, String requestMethod, String consumerScrete,
                                 Map<String, String> params) {
        String sign = "";
        if (StringUtils.isEmpty(url) || CollectionUtil.isEmpty(params)) {
            return sign;
        }
        // 计算签名值
        try {
            // 格式化参数键值对
            List<String> list = new ArrayList<String>();
            for (String key : params.keySet()) {
            	//String valueString = params.get(key)==null?key + "=":key + "="+params.get(key);
                list.add(key + "="+params.get(key));
            }
            // 对格式化好的参数键值以字典序升序排列，然后拼接在一起
            Collections.sort(list);
            // 存储所有需要计算前面值的字符串
            StringBuilder sb = new StringBuilder();
            sb.append(requestMethod.toUpperCase());
            // 计算签名的时候需要截取url字符串，只将：之前的字段和target|去掉url中的端口号(仅计算签名值得时候)
            sb.append(url);
            for (String kv : list) {
                sb.append(kv);
            }
            sb.append(consumerScrete);
            System.out.println("local sign:"+sb.toString());
            sign = MD5Util.calcMD5(URLEncoder.encode(sb.toString(), Consts.UTF8));
            LOG.info("The sign value is {}.", sign);
        } catch (UnsupportedEncodingException e) {
            LOG.error("", e);
        }
        return sign;
    }
}
