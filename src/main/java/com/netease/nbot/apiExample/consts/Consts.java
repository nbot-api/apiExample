package com.netease.nbot.apiExample.consts;

/**
 * @date 2017年2月13日
 * @author hzlipengzhen@corp.netease.com
 */
public class Consts {

    public static final String UTF8 = "UTF-8";
    public static final String PULL = "pull";
    // 私钥对应的参数名
    public static final String SECRET_KEY = "consumerScrete";
    // 签名值参数名
    public static final String SIGN = "sign";
    public static final String MD5 = "MD5";
    public static final String CONSUMER_KEY = "consumerKey";
    public static final String TIME_STAMP = "timestamp";
    public static final String EXPIRES = "expires";
    
}
