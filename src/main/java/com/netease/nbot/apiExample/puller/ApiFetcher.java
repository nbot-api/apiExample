package com.netease.nbot.apiExample.puller;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.netease.nbot.apiExample.consts.Consts;
import com.netease.nbot.apiExample.meta.Parameter;
import com.netease.nbot.apiExample.sign.SignCalculator;
import com.netease.nbot.apiExample.util.CollectionUtil;
import com.netease.nbot.apiExample.util.FileUtil;


/**
 * 文章拉取api完整使用范例
 * 
 * @date 2017年2月1日
 * @author hzlipengzhen@corp.netease.com
 */
public class ApiFetcher {
    private static final Logger LOG = LoggerFactory.getLogger(ApiFetcher.class);
    private static final int TIME_OUT = 5000;
    // 需要拉取的产品公钥，需求方提供
    private static final String CONSUMER_KEY = "******";
    // 需要拉取的产品私钥，需求方提供
    private static final String SECRET_KEY = "******";
    // 产品id，也可通过接口获取详细信息
    private static final long PRODUCT_ID = 43;

    /**
     * 执行请求，返回结果
     * 
     * @param url 接口url
     * @param parameters 参数列表，包括公有参数和接口请求参数
     * @return
     */
    private static String getResponse(String url, List<Parameter> parameters) {
        HttpClient client = new HttpClient();
        client.getHttpConnectionManager().getParams().setConnectionTimeout(TIME_OUT);
        GetMethod getMethod = null;
        String responseStr = "";
        try {
            getMethod = new GetMethod(url);
            getMethod.getParams().setContentCharset(Consts.UTF8);
            // 请求参数
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            Map<String, String> paramsMap = new HashMap<String, String>();
            if (CollectionUtil.isEmpty(parameters)) {
                return "";
            }
            Parameter consumer = null;
            for (Parameter p : parameters) {
                // 注意：私钥不放入url参数中，但是计算签名值要用到该参数
                if (p.getName().equals(Consts.SECRET_KEY)) {
                    consumer = p;
                    continue;
                }
                params.add(new NameValuePair(p.getName(), p.getValue()));
                paramsMap.put(p.getName(), p.getValue());
            }
            String consumerScrete = consumer.getValue();
            String getSign =
                    SignCalculator
                            .getSign(url, getMethod.getName(), consumerScrete, paramsMap);
            // 将签名值加入url的参数中
            params.add(new NameValuePair(Consts.SIGN, getSign));
            getMethod.setQueryString(params.toArray(new NameValuePair[0]));
            int status = client.executeMethod(getMethod);
            if (status == HttpStatus.SC_OK) {
                responseStr = getMethod.getResponseBodyAsString();
            }
            System.out.println("status="+status);
        } catch (Throwable th) {
        	th.printStackTrace();
            LOG.error("", th);
        } finally {
            if (getMethod != null) {
                getMethod.releaseConnection();
            }
        }
        return responseStr;
    }

    /**
     * 公有参数列表
     * 
     * @return
     */
    private static List<Parameter> getPublicParams() {
        List<Parameter> parameters = new ArrayList<Parameter>();
        // 公钥
        Parameter parameter = new Parameter();
        parameter.setName(Consts.CONSUMER_KEY);
        parameter.setValue(CONSUMER_KEY);
        parameters.add(parameter);
        // 私钥
        parameter = new Parameter();
        parameter.setName(Consts.SECRET_KEY);
        parameter.setValue(SECRET_KEY);
        parameters.add(parameter);
        // 时间戳 /十分钟有效
        parameter = new Parameter();
        parameter.setName(Consts.TIME_STAMP);
        parameter.setValue(String.valueOf(System.currentTimeMillis()));
        parameters.add(parameter);
        // 过期时间 这个参数非必需
        parameter = new Parameter();
        parameter.setName(Consts.EXPIRES);
        parameter.setValue(String.valueOf(System.currentTimeMillis() + 5 * 60 * 1000));
        parameters.add(parameter);
        return parameters;
    }

    /**
     * 获取某个产品下的源|种子列表 对应接口：http://api.nbot.netease.com/pull/source/list_all/ids.json
     * 
     * @param PRODUCT_ID 产品id
     * @param page 非必须 页码
     * @param count 非必须 单页返回的数量
     * @throws IOException
     */
    public String getSourceList(long productId, int page, int count) throws IOException {
        String url = "http://api.nbot.netease.com/pull/source/list/ids.json";
        // 获取公有参数，公钥、私钥、时间戳、过期时间等
        List<Parameter> params = getPublicParams();
        // 产品参数 name必须为PRODUCT_ID
        Parameter param = new Parameter();
        param.setName("productId");
        param.setValue(String.valueOf(productId));
        params.add(param);
        // 返回结果的页码，默认为1
        int pag = page <= 0 ? 1 : page;
        param = new Parameter();
        param.setName("page");
        param.setValue(String.valueOf(pag));
        params.add(param);
        // 单页返回的记录条数，最大不超过100，默认为20
        int cn = count >= 100 ? 100 : count;
        cn = cn <= 0 ? 20 : cn;
        param = new Parameter();
        param.setName("count");
        param.setValue(String.valueOf(cn));
        params.add(param);
        // json型字符串
        String resp = getResponse(url, params);
        System.out.println(resp);
        return resp;
    }

    /**
     * 获取指定的某个产品下的指定源信息 对应接口：http://api.nbot.netease.com/pull/source/info.json
     * 
     * @param seedId 源id
     * @param PRODUCT_ID 产品id 确保要获取的源是该产品下的源
     * @throws IOException
     */
    public String getSourceInfo(long productId, long seedId) throws IOException {
        String url = "http://api.nbot.netease.com/pull/source/info.json";
        // 获取公有参数，公钥、私钥、时间戳、过期时间等
        List<Parameter> params = getPublicParams();
        // 同testGetSourceList方法
        Parameter param = new Parameter();
        param.setName("productId");
        param.setValue(String.valueOf(productId));
        params.add(param);
        // 种子|源参数，name为seedId
        param = new Parameter();
        param.setName("seedId");
        param.setValue(String.valueOf(seedId));
        params.add(param);
        String resp = getResponse(url, params);
        System.out.println(resp);
        return resp;
    }

    /**
     * 获取指定的某个产品下的指定源最后更新的文章的时间 对应接口：http://api.nbot.netease.com/pull/source/lastUpdateTime.json
     * 
     * @param userCustomizedId 用户自定义标识，具有唯一性
     * @throws IOException
     */
    public String getSourceLastUpdate(String userCustomizedId) throws IOException {
        String url = "http://api.nbot.netease.com/pull/source/lastUpdateTime.json";
        // 获取公有参数，公钥、私钥、时间戳、过期时间等
        List<Parameter> params = getPublicParams();
        // 用户自定义标识字段
        Parameter param = new Parameter();
        param.setName("userCustomizedId");
        param.setValue(userCustomizedId);
        params.add(param);
        String resp = getResponse(url, params);
        System.out.println(resp);
        return resp;
    }

    /**
     * 获取本产品指定源的指定时间点后新抓取的文章记录 对应接口：http://api.nbot.netease.com/pull/source/timeline/recordIds.json
     * 
     * @param PRODUCT_ID 产品id
     * @param seedId 源id
     * @param page 非必须
     * @param count 非必须
     * @param since_time 指定的时间点，非必须，若不填则默认为0
     * @throws IOException
     */
    public String getRecordsByTimeLine(long productId, long seedId, int page, int count,
                                       long since_time) throws IOException {
        String url = "http://api.nbot.netease.com/pull/source/timeline/recordIds.json";
        List<Parameter> params = getPublicParams();

        Parameter param = new Parameter();
        param.setName("productId");
        param.setValue(String.valueOf(productId));
        params.add(param);

        param = new Parameter();
        param.setName("seedId");
        param.setValue(String.valueOf(seedId));
        params.add(param);

        // 返回结果的页码，默认为1
        int pag = page <= 0 ? 1 : page;
        param = new Parameter();
        param.setName("page");
        param.setValue(String.valueOf(pag));
        params.add(param);
        // 单页返回的记录条数，最大不超过100，默认为20
        int cn = count >= 100 ? 100 : count;
        cn = cn <= 0 ? 20 : cn;
        param = new Parameter();
        param.setName("count");
        param.setValue(String.valueOf(cn));
        params.add(param);
        // 若指定此参数，则返回createTime比since_time大的记录（即比since_time时间晚的记录），默认为0
        param = new Parameter();
        param.setName("since_time");
        param.setValue(String.valueOf(since_time));
        params.add(param);
        String resp = getResponse(url, params);
        System.out.println(resp);
        return resp;
    }

    /**
     * 获取指定的文章记录的详细信息 对应接口：http://api.nbot.netease.com/pull/record/detail.json
     * 
     * @param recordId 记录id
     * @throws IOException
     */
    public String getRecordDetail(long recordId) throws IOException {
        String url = "http://api.nbot.netease.com/pull/record/detail.json";
        // 获取公有参数，公钥、私钥、时间戳、过期时间等
        List<Parameter> params = getPublicParams();
        // 具体的文章记录id，name为recordId
        Parameter param = new Parameter();
        param.setName("recordId");
        param.setValue(String.valueOf(recordId));
        params.add(param);
        String resp = getResponse(url, params);
        System.out.println(resp);
        return resp;
    }


    /**
     * 根据用户自定义标识获取某个时间点之后的文章列表 对应接口：http://api.nbot.netease.com/pull/source/getRecords.json
     * 
     * @param userCustomizedId 用户自定义标识
     * @param since_time 选取的时间点
     * @param count 获取的文章数量 最大只能是100
     * @throws IOException
     */
    public String getRecordsByUserCustomizedId(String userCustomizedId, long since_time,
                                               int count, int page) throws IOException {
        String record = "";
        String uri = "http://api.nbot.netease.com/pull/source/getRecords.json";
        List<Parameter> params = getPublicParams();
        // 接口参数
        Parameter param = new Parameter("userCustomizedId", userCustomizedId);
        params.add(param);
        param = new Parameter("sinceTime", String.valueOf(since_time));
        params.add(param);
        param = new Parameter("count", String.valueOf(count));
        params.add(param);
        param = new Parameter("page", String.valueOf(page));
        params.add(param);
        record = getResponse(uri, params);
        System.out.println(record);
        return record;
    }


    /**
     * 获取指定的recordId的信息，使用公钥和私钥获取指定产品的信息 对应接口:http://api.nbot.netease.com/pull/product/info.json
     * 
     * @throws IOException
     */
    public String getProductInfo() throws IOException {
        String url = "http://localhost:8080/nbot-api-0.1.0/pull/product/info.json";
        // 获取公有参数，公钥、私钥、时间戳、过期时间等
        List<Parameter> params = getPublicParams();
        String resp = getResponse(url, params);
        System.out.println(resp);
        return resp;
    }


    /**
     * 根据用户自定义标识获取对应的源id信息 对应接口：http://api.nbot.netease.com/pull/source/id.json
     * 
     * @param userCustomizedId 用户自定义标识
     * @throws IOException
     */
    public String getSourceIdByUserCustomizedId(String userCustomizedId) throws IOException {
        String record = "";
        String uri = "http://api.nbot.netease.com/pull/source/id.json";
        List<Parameter> params = getPublicParams();
        // 接口参数
        Parameter param = new Parameter();
        param.setName("userCustomizedId");
        param.setValue(userCustomizedId);
        params.add(param);

        record = getResponse(uri, params);
        System.out.println(record);
        return record;
    }

    /**
     * 根据抓取时间区域获取本产品指定源的最新的记录列表 {since_time, max_time}
     * 
     * @throws IOException
     */
    public String getRecordsByFetchTimeLine(long productId, long seedId, long sinceTime,
                                            long endTime, int count, int page)
            throws IOException {
        String url = "http://api.nbot.netease.com/pull/source/fetch_timeline/recordIds.json";
        List<Parameter> params = getPublicParams();

        Parameter param = new Parameter();
        param.setName("productId");
        param.setValue(String.valueOf(productId));
        params.add(param);

        param = new Parameter();
        param.setName("seedId");
        param.setValue(String.valueOf(seedId));
        params.add(param);
        // 指定的起始时间
        param = new Parameter();
        param.setName("since_time");
        param.setValue(String.valueOf(sinceTime));
        params.add(param);
        // 指定的终止时间
        param = new Parameter();
        param.setName("max_time");
        param.setValue(String.valueOf(endTime));
        params.add(param);
        // 每页获取的数量
        param = new Parameter("count", String.valueOf(count));
        params.add(param);
        // 指定的页数
        param = new Parameter("page", String.valueOf(page));
        params.add(param);

        String resp = getResponse(url, params);
        System.out.println(resp);
        return resp;
    }
    
    /*
     * 删除指定记录
     * @param productId   产品id
     * @param seedId        内容源id
     * @param recordId     记录id
     */
    public String deleteRecord(long productId,long seedId,long recordId){
    	String url = "http://api.nbot.netease.com/pull/record/delete.json";
        List<Parameter> params = getPublicParams();
        Parameter param = new Parameter();
        param.setName("productId");
        param.setValue(String.valueOf(productId));
        params.add(param);
        param = new Parameter();
        param.setName("seedId");
        param.setValue(String.valueOf(seedId));
        params.add(param);
        param = new Parameter();
        param.setName("recordId");
        param.setValue(String.valueOf(recordId));
        params.add(param);
        String resp = getResponse(url, params);
        System.out.println(resp);
        return resp;
    }
    
    
    public String addSource(/*String consumerKey, */String productId, String name,
            String uri, String type,String alias) throws IOException {
        String url = "http://api.nbot.netease.com/pull//source/add.json";
        List<Parameter> params = getPublicParams();
        /*Arrays.asList("consumerKey",
                "productId", "name", "uri", "type", "alias", "refetchedTime", "priority",
                "userCustomizedId", "callbackurl")*/
        Parameter param = new Parameter();
        /*param.setName("consumerKey");
        param.setValue(consumerKey);
        params.add(param);*/
        param = new Parameter();
        param.setName("productId");
        param.setValue(productId);
        params.add(param);
        param = new Parameter();
        param.setName("name");
        param.setValue(name);
        params.add(param);
        param = new Parameter();
        param.setName("uri");
        param.setValue(uri);
        params.add(param);
        param = new Parameter();
        param.setName("type");
        param.setValue(type);
        params.add(param);
        param = new Parameter();
        param.setName("alias");
        param.setValue(alias);
        params.add(param);
        param = new Parameter();
        param.setName("refetchedTime");
        param.setValue("1");
        params.add(param);
        param = new Parameter();
        param.setName("priority");
        param.setValue("1");
        params.add(param);
        param = new Parameter();
        param.setName("userCustomizedId");
        param.setValue("1");
        params.add(param);
        param = new Parameter();
        param.setName("callbackurl");
        param.setValue("1");
        params.add(param);
        
        
        String resp = getResponse(url, params);
        System.out.println(resp);
        return resp;
    }
    


    public static void main(String[] args) throws Exception {
        ApiFetcher fetcher = new ApiFetcher();
        //fetcher.getProductInfo();
        try {
        	fetcher.getProductInfo();
            /*
        	// 获取产品信息
            fetcher.getProductInfo();
            // 获取源列表信息
            fetcher.getSourceList(PRODUCT_ID, 1, 20);
            // 获取指定源信息
            fetcher.getSourceInfo(PRODUCT_ID, 32596);
            // 获取指定源最近更新时间
            fetcher.getSourceLastUpdate("share_3");
            // 获取指定源指定时间后抓取的文章记录id列表
            fetcher.getRecordsByTimeLine(PRODUCT_ID, 32079, 1, 20, 0);
            // 分页获取指定源指定时间范围内抓取的文章列表
            fetcher.getRecordsByFetchTimeLine(PRODUCT_ID, 32596, System.currentTimeMillis(),
                    System.currentTimeMillis(), 100, 1);
            // 获取指定用户自定义标识下新更新的文章id列表
            fetcher.getRecordsByUserCustomizedId("wyh_1178393",
                    System.currentTimeMillis() - 256 * 3600 * 1000, 6, 1);
            // 获取指定文章详情
            fetcher.getRecordDetail(23107459);
            // 根据用户自定义标识id获取对应的源id信息
            fetcher.getSourceIdByUserCustomizedId("share_3");
         
            //删除指定记录，慎重使用
            //fetcher.deleteRecord(48l, 57965l, 52015837l);
        	
        } catch (Throwable th) {
        	th.printStackTrace();
            LOG.error("", th);
        }
    }
}
