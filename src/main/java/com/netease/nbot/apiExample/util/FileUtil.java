package com.netease.nbot.apiExample.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/** 
 * 文件工具类，包含文件读写操作
 * @author    hzchenkaimin
 * @date       2017年6月28日
 */
public class FileUtil {
	
	public static String readString(String filePath) throws IOException{
		FileReader fReader = new FileReader(filePath);
		BufferedReader bReader = new BufferedReader(fReader);
		StringBuilder sBuilder = new StringBuilder();
		String line = null;
		try{
			while((line = bReader.readLine())!=null){
				//System.out.println(line);
				sBuilder.append(line);
			}
		}finally{
			if(bReader != null){
				bReader.close();
			}
		}
		return sBuilder.toString();
	}
	
	public static List<String> readList(String filePath) throws IOException{
		InputStreamReader sReader = new InputStreamReader(new FileInputStream(filePath));
		BufferedReader bReader = new BufferedReader(sReader);
		List<String> contents = new ArrayList<String>();
		String line = null;
		try{
			while((line = bReader.readLine())!=null){
				System.out.println(line);
				contents.add(line);
			}
		}finally{
			if(bReader != null){
				bReader.close();
			}
		}
		return contents;
	}
	
	public static byte[] readBytes(InputStream inputStream) throws IOException{
		ByteArrayOutputStream swapStream = new ByteArrayOutputStream();  
        byte[] buff = new byte[1024];  
        int rc = 0;  
        while ((rc = inputStream.read(buff, 0, 1024)) > 0) {  
            swapStream.write(buff, 0, rc);  
        }  
        return swapStream.toByteArray();  
	}
	
	/**
	 * 写文件
	 * @param filePath      文件路径
	 * @param content      需要写入的内容
	 * @param append      是否采用最佳形式
	 * @throws IOException
	 */
	public static void write(String filePath,String content,boolean append) throws IOException{
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(new File(filePath), append);
			fileWriter.write(content);
		} finally {
			if(fileWriter != null){
				fileWriter.close();
			}
		}
	}
	
	public static void write(String filePath,byte[] bytes){
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(new File(filePath));
			out.write(bytes);
		} catch(Exception e){
			e.printStackTrace();
		}finally {
			if(out != null){
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static String getJarPath(){
		URL url = FileUtil.class.getProtectionDomain().getCodeSource()
                .getLocation();
        String filePath = null;
        try {
            filePath = java.net.URLDecoder.decode(url.getPath(), "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (filePath.endsWith(".jar"))
            filePath = filePath.substring(0, filePath.lastIndexOf("/") + 1);
        File file = new java.io.File(filePath);
        filePath = file.getAbsolutePath();
        return filePath;
	}

}
