package com.netease.nbot.apiExample.util;

import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

/**
 * @date 2017年2月13日
 * @author hzlipengzhen@corp.netease.com
 */
public class CollectionUtil extends CollectionUtils {

    public static <K, V> boolean isEmpty(Map<K, V> map) {
        return map == null || map.isEmpty();
    }

    public static <K, V> boolean isNotEmpty(Map<K, V> map) {
        return !isEmpty(map);
    }

}
