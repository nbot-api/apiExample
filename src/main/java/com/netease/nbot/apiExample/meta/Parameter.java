 package com.netease.nbot.apiExample.meta; /** 
/**
 * 请求参数
 * 
 * @date 2017年2月8日
 * @author hzlipengzhen@corp.netease.com
 */
public class Parameter {
    private String name;
    private String value;

    public Parameter() {
        super();
    }

    /**
     * @param name
     * @param value
     */
    public Parameter(String name, String value) {
        super();
        this.name = name;
        this.value = value;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
}
